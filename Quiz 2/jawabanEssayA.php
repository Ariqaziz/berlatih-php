<?php
    function hitung($string_data) {
        $array = strval($string_data);
        
        if ($array[1] == "+") {
            $a = (int)$array[0];
            $b = (int)$array[2];
            echo $a + $b;
        }

        elseif ($array[2] == "-") {
            $c = (int)$array[0] . (int)$array[1];
            $d = (int)$array[3];
            echo $c - $d;
        }

        elseif ($array[3] == "*") {
            $e = (int)$array[0] . (int)$array[1] . (int)$array[2];
            $f = (int)$array[4];
            echo $e * $f;
        }

        elseif ($array[3] == ":") {
            $g = (int)$array[0] . (int)$array[1] . (int)$array[2];
            $h = (int)$array[4] . (int)$array[5];
            echo $g / $h;
        }

        elseif ($array[3] == "%") {
            $i = (int)$array[0] . (int)$array[1] . (int)$array[2];
            $j = (int)$array[4];
            echo $i % $j;
        }
        echo "<br>";
    }

    //TEST CASES
    echo hitung("102*2"); //204
    echo hitung("2+3"); //5
    echo hitung("100:25"); //4
    echo hitung("100%2"); //0
    echo hitung("99-2"); //97

?>